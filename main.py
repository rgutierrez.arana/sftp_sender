from genericpath import isdir
from connectors import ssh_connector
import os
import argparse

DEFAULT_MSG ="if no provided it will use the one from the enviroment variables"

parser = argparse.ArgumentParser(description="Send files over sftp to a server")
parser.add_argument("--remote_dir" ,type=str , help="remote dir of the SFTP server" , default="/sftp_user/test_dir" )
parser.add_argument("--file", type=str, help="send only one file")
parser.add_argument("--dir", type=str, help="send entire directory")



parser.add_argument("--hostname", type=str,help=f"hostname of the SFTP server {DEFAULT_MSG}")
parser.add_argument("--port", type=str,help=f"port of the SFTP server {DEFAULT_MSG}" ,default="22")
parser.add_argument("--user_name", type=str,help=f"user_name of the SFTP server {DEFAULT_MSG}")
parser.add_argument("--password", type=str,help=f"password of the SFTP server {DEFAULT_MSG}")
parser.add_argument("--key", type=str,help=f"key of the SFTP server it will not load the one from the env vars")


if __name__ == "__main__":
  args=parser.parse_args()

  hostname: str = os.getenv("hostname", args.hostname)
  port: int = int(os.getenv("port", args.port))
  user_name: str = os.getenv("user_name", args.user_name)
  password: str = os.getenv("password", args.password)
  key: str = os.getenv("key", args.key)
  
  path_to_transfer= []
  
  if args.file:
    path_to_transfer = [(args.file, args.remote_dir),]
  elif args.dir:
    if not os.path.isdir(args.dir):
      raise Exception(f"the element {args.dir} is not a valid directory")
    os.chdir(args.dir)
    
    for file in os.listdir():
      path_to_transfer.append((file , args.remote_dir))
    


  client_ssh = ssh_connector.SshConnector(
      hostname=hostname,
      port=port,
      user_name=user_name,
      password=password,
      key=key,
      paths_to_transfer=path_to_transfer
  )

  client_ssh.connect()
  try:
    responses = client_ssh.run()
  finally:

    client_ssh.disconnect()
