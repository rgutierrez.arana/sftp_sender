from dataclasses import dataclass , field
from typing import Dict, List, Tuple
from connectors import base_connector
import paramiko
from pprint import pprint
import os
import pathlib 
@dataclass
class SshConnector(base_connector.BaseConnector):

  connector: paramiko.SSHClient = None
  paths_to_transfer :List[Tuple[str , str]] = field(default_factory=[])

  def connect(self)->paramiko.SSHClient:

    self.connector = paramiko.SSHClient()
    optional_args = {}
    if self.key:
      optional_args["key"] = self.key
    self.connector.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    self.connector.connect(hostname=self.hostname,
                           port=self.port,
                           username=self.user_name,
                           password=self.password,
                           **optional_args
                           )

    self.connected = True    
    
  def disconnect(self):
    if self.connected:
      self.connector.close()


  def run(self)->List[dict]:
    transfered_files = []
    for path_to_transfer in self.paths_to_transfer:
      local_path , remote_path = path_to_transfer
      if os.path.isfile(local_path):
        
        transfered=self.send_file(path_file=local_path , remote_path=remote_path)
        transfered_files.append(
          {
            "path" : local_path,
            "transfered" : transfered,
            
          }
        )
      else:
        transfered_files.append({
          "path" : local_path,
          "transfered" : False
        })
        
    print("*"*40 , " TRANSFERED " , "*"*40)
    print("*"*100)
    pprint(transfered_files)
    print()
    print("*"*100)
    
    return transfered_files
      
      
      
  def status_send(self,transfered : int , total_to_be_transfered:int)->None:
    print(f"transfered = {transfered} | total_to_be_transfered = {total_to_be_transfered}")
    
  
  def send_file(self,path_file:str , remote_path:str)->bool:
    
    filename = pathlib.Path(path_file).name
    new_remote_path = f"{remote_path}/{filename}"
    
    print(f"path_file = {path_file}")
    print(f"remote_path = {remote_path}")
    print(f"new_remote_path = {new_remote_path}")
    
    
    
    if not self.connected:
      self.connect()
    try:
      
      sftp_client: paramiko.SFTPClient = self.connector.open_sftp()
      sftp_client.put(localpath=path_file,
                      remotepath=new_remote_path,
                      callback=self.status_send)
      return True
    except Exception as ex:
      import traceback as tb
      
      tb.print_exc()
      return False
    finally:
      sftp_client.close()
    
