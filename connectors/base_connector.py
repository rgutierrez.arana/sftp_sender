from dataclasses import dataclass , asdict
from abc import ABC, abstractmethod
from typing import Optional


@dataclass
class BaseConnector(ABC):

  hostname: str = ""
  port: int = 9999
  user_name:str =""
  password:str = ""
  key: Optional[str] = ""
  connected:bool = False
  @abstractmethod
  def connect(self): ...

  @abstractmethod
  def disconnect(self): ...

  @abstractmethod
  def run(self): ...
  
  
  
  
  
  
