# sftp_sender

## installing dependencies

python -m pip install requirements.txt

## Usage


usage: main.py [-h] [--remote_dir REMOTE_DIR] [--file FILE] [--dir DIR]       
               [--hostname HOSTNAME] [--port PORT] [--user_name USER_NAME]    
               [--password PASSWORD] [--key KEY]

Send files over sftp to a server

optional arguments:
  -h, --help            show this help message and exit
  --remote_dir REMOTE_DIR
                        remote dir of the SFTP server -> ex: --remote="sftp_user/test_folder"
  --file FILE           send only one file -> ex: --file="/home/test_user/test_file.txt"
  --dir DIR             send entire directory -> ex: --dir="/home/test_user/test_folder"

  --hostname HOSTNAME   hostname of the SFTP server if no provided it will use
                        the one from the enviroment variables -> ex: hostname="192.168.1.12"
  --port PORT           port of the SFTP server if no provided it will use the
                        one from the enviroment variables -> ex: --port=22
  --user_name USER_NAME
                        user_name of the SFTP server if no provided it will   
                        use the one from the enviroment variables -> ex: --user_name="test_user"
  --password PASSWORD   password of the SFTP server if no provided it will use
                        the one from the enviroment variables ex: --password="ieriofj131c"
  --key KEY             key of the SFTP server it will not load the one from  
                        the env vars ex: --key="/home/keys/rsa_key"

example 

## In this case it will send one file to the corresponding SFTP server and using the env vars to set the 
## [hostname , port , user_name , poassword]
python3 --remote="/home/test_user/test_folder" --file="/home/local_user/test_file.txt"
